package info.earty.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StatelessGatewayApplication {
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(StatelessGatewayApplication.class);
		application.run(args);
	}
}
