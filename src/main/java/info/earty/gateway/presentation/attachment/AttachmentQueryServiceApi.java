package info.earty.gateway.presentation.attachment;

import info.earty.gateway.infrastructure.jaxrs.cxf.CxfServiceApiProxy;
import info.earty.gateway.presentation.common.JaxRsProxyError;
import info.earty.attachment.presentation.AttachmentQueryApi;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;

@Service
@RequiredArgsConstructor
@JaxRsProxyError
public class AttachmentQueryServiceApi implements AttachmentQueryApi, CxfServiceApiProxy {

    private final AttachmentQueryApi attachmentQueryApi;

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public Response get(String attachmentId) {
        return attachmentQueryApi.get(attachmentId);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public Response get(String attachmentId, String filename) {
        return attachmentQueryApi.get(attachmentId, filename);
    }

    @Override
    public Response getPublished(String attachmentId, String workingCardId) {
        return attachmentQueryApi.getPublished(attachmentId, workingCardId);
    }

    @Override
    public Response getPublished(String attachmentId, String filename, String workingCardId) {
        return attachmentQueryApi.getPublished(attachmentId, filename, workingCardId);
    }
}
