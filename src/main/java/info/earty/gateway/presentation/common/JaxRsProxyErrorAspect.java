package info.earty.gateway.presentation.common;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

//NOTE EA 9/10/2021 - If a proxy request to the backend service returns an unexpected response (3xx, 4xx, 5xx); then
//  if we simply allow the response to propagate through to the true client they end up getting a response that looks like
//      http: error: ChunkedEncodingError: ('Connection broken:
//      InvalidChunkLength(got length b\'{"message":"{partial or complete original error message\', 0 bytes read)',
//      InvalidChunkLength(got length b'{"message":"partial or complete original error message', 0 bytes read))
//  Additionally I notice that the transfer-encoding is set as a header as 'chunked' if we don't apply this advice.
//  My theory as to why this is, is because the proxy has already read in that error response and so when it gets to
//  the actual client it appears as though its being read in again resulting in an error. So this advice essentially refreshes
//  the response as a new response so that the true client can behave like it is reading it in for the first time.
@Aspect
@Component
@RequiredArgsConstructor
public class JaxRsProxyErrorAspect {

    @Around("@annotation(jaxRsProxyError))")
    public Object jaxRsProxyErrorAroundAdvice(ProceedingJoinPoint joinPoint,
                                                        JaxRsProxyError jaxRsProxyError) throws Throwable {
        return this.jaxRsProxyErrorAdvice(joinPoint, jaxRsProxyError);
    }

    @Around("@within(jaxRsProxyError) && !@annotation(info.earty.gateway.presentation.common.JaxRsProxyError)")
    public Object jaxRsProxyErrorWithinAdvice(ProceedingJoinPoint joinPoint,
                                                        JaxRsProxyError jaxRsProxyError) throws Throwable {
        return this.jaxRsProxyErrorAdvice(joinPoint, jaxRsProxyError);
    }

    private Object jaxRsProxyErrorAdvice(ProceedingJoinPoint joinPoint,
                                         JaxRsProxyError jaxRsProxyError) throws Throwable {
        Object result;
        try {
            result = joinPoint.proceed();
        } catch (WebApplicationException webApplicationException) {
            Response original = webApplicationException.getResponse();
            Response proxied = Response.status(original.getStatus())
                    .entity(original.getEntity())
                    .type(original.getMediaType()).build();
            throw new WebApplicationException(proxied);
        }

        //NOTE EA 2022-04-18 - Adding special handling for results of type Response since it appears as though the cxf
        //  implementation still puts it into a state where the true client sees just a generic error instead of the expected
        //  json error dto and with a Response return type it will not throw a WebApplicationException.
        if (result instanceof Response) {
            Response response = (Response) result;
            if (response.getStatus() >= 200 && response.getStatus() < 299) {
                return response;
            }
            else {
                Response proxied = Response.status(response.getStatus())
                        .entity(response.getEntity())
                        .type(response.getMediaType()).build();
                throw new WebApplicationException(proxied);
            }
        }
        else {
            return result;
        }
    }

}
