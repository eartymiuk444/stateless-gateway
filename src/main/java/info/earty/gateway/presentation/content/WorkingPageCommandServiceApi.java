package info.earty.gateway.presentation.content;

import info.earty.content.presentation.WorkingPageCommandApi;
import info.earty.content.presentation.command.page.*;
import info.earty.gateway.infrastructure.jaxrs.cxf.CxfServiceApiProxy;
import info.earty.gateway.presentation.common.JaxRsProxyError;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@PreAuthorize("hasRole('ADMIN')")
@JaxRsProxyError
public class WorkingPageCommandServiceApi implements WorkingPageCommandApi, CxfServiceApiProxy {

    private final WorkingPageCommandApi workingPageCommandApi;

    @Override
    public void addOutlineItem(AddOutlineItemJsonCommand jsonCommand) {
        workingPageCommandApi.addOutlineItem(jsonCommand);
    }

    @Override
    public void changeOutlineItemFragment(ChangeOutlineItemFragmentJsonCommand jsonCommand) {
        workingPageCommandApi.changeOutlineItemFragment(jsonCommand);
    }

    @Override
    public void removeOutlineItem(RemoveOutlineItemJsonCommand jsonCommand) {
        workingPageCommandApi.removeOutlineItem(jsonCommand);
    }

    @Override
    public void moveOutlineItem(MoveOutlineItemJsonCommand jsonCommand) {
        workingPageCommandApi.moveOutlineItem(jsonCommand);
    }

    @Override
    public void moveOutlineSubItemUp(MoveOutlineSubItemUpJsonCommand jsonCommand) {
        workingPageCommandApi.moveOutlineSubItemUp(jsonCommand);
    }

    @Override
    public void moveOutlineSubItemDown(MoveOutlineSubItemDownJsonCommand jsonCommand) {
        workingPageCommandApi.moveOutlineSubItemDown(jsonCommand);
    }

    @Override
    public void discardDraft(DiscardDraftJsonCommand jsonCommand) {
        workingPageCommandApi.discardDraft(jsonCommand);
    }

    @Override
    public void publish(PublishJsonCommand jsonCommand) {
        workingPageCommandApi.publish(jsonCommand);
    }

    @Override
    public void changeTitle(ChangeTitleJsonCommand jsonCommand) {
        workingPageCommandApi.changeTitle(jsonCommand);
    }

    @Override
    public void changeOutlineItemTitle(ChangeOutlineItemTitleJsonCommand jsonCommand) {
        workingPageCommandApi.changeOutlineItemTitle(jsonCommand);
    }
}
