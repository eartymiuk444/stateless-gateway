package info.earty.gateway.presentation.content;

import info.earty.content.presentation.WorkingCardQueryApi;
import info.earty.content.presentation.data.DraftCardJsonDto;
import info.earty.content.presentation.data.PublishedCardJsonDto;
import info.earty.gateway.infrastructure.jaxrs.cxf.CxfServiceApiProxy;
import info.earty.gateway.presentation.common.JaxRsProxyError;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor
@JaxRsProxyError
public class WorkingCardQueryServiceApi implements WorkingCardQueryApi, CxfServiceApiProxy {

    private final WorkingCardQueryApi workingCardQueryApi;


    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public DraftCardJsonDto getDraftCard(String workingCardId) {
        return workingCardQueryApi.getDraftCard(workingCardId);
    }

    @Override
    public PublishedCardJsonDto getPublishedCard(String workingCardId) {
        return workingCardQueryApi.getPublishedCard(workingCardId);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public Set<DraftCardJsonDto> getDraftCardsByWorkingPageId(String workingPageId) {
        return workingCardQueryApi.getDraftCardsByWorkingPageId(workingPageId);
    }

    @Override
    public Set<PublishedCardJsonDto> getPublishedCardsByWorkingPageId(String workingPageId) {
        return workingCardQueryApi.getPublishedCardsByWorkingPageId(workingPageId);
    }
}
