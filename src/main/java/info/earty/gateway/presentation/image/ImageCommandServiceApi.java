package info.earty.gateway.presentation.image;

import info.earty.gateway.infrastructure.jaxrs.cxf.CxfServiceApiProxy;
import info.earty.gateway.presentation.common.JaxRsProxyError;
import info.earty.image.presentation.ImageCommandApi;
import lombok.RequiredArgsConstructor;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@PreAuthorize("hasRole('ADMIN')")
@JaxRsProxyError
public class ImageCommandServiceApi implements ImageCommandApi, CxfServiceApiProxy {

    private final ImageCommandApi imageCommandApi;

    @Override
    public void create(Attachment attachment, String workingCardId) {
        imageCommandApi.create(attachment, workingCardId);
    }
}
