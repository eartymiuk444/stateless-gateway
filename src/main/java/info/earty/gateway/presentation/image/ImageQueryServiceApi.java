package info.earty.gateway.presentation.image;

import info.earty.gateway.infrastructure.jaxrs.cxf.CxfServiceApiProxy;
import info.earty.gateway.presentation.common.JaxRsProxyError;
import info.earty.image.presentation.ImageQueryApi;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;

@Service
@RequiredArgsConstructor
@JaxRsProxyError
public class ImageQueryServiceApi implements ImageQueryApi, CxfServiceApiProxy {

    private final ImageQueryApi imageQueryApi;

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public Response get(String imageId) {
        return imageQueryApi.get(imageId);
    }

    @Override
    public Response getPublished(String imageId, String workingCardId) {
        return imageQueryApi.getPublished(imageId, workingCardId);
    }
}
