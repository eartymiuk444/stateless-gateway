package info.earty.gateway.presentation.user;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.HashSet;
import java.util.stream.Collectors;

@Service
@Path("/user/")
public class UserService {

    @GET
    @Path("/details")
    @Produces(MediaType.APPLICATION_JSON)
    public UserDetailsDto details() {
        UserDetailsDto dto = new UserDetailsDto();

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            dto.setUserName(authentication.getName());
            dto.setAuthorities(authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet()));
        }
        else {
            dto.setUserName("ANONYMOUS");
            dto.setAuthorities(new HashSet<>());
        }

        return dto;
    }

}
