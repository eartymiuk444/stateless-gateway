package info.earty.gateway.infrastructure.jaxrs.cxf;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import info.earty.content.presentation.*;
import lombok.RequiredArgsConstructor;
import org.apache.cxf.jaxrs.client.JAXRSClientFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@RequiredArgsConstructor
@Configuration
public class ContentClientConfiguration {

    @Value("${earty.info.content.service.host}")
    private String contentServiceHost;

    @Value("${earty.info.content.service.port}")
    private String contentServicePort;

    private final JacksonJsonProvider jacksonJsonProvider;

    @Bean
    public SiteMenuCommandApi siteMenuCommandApi() {
        return clientApiHelper(SiteMenuCommandApi.class);
    }

    @Bean
    public SiteMenuQueryApi siteMenuQueryApi() {
        return clientApiHelper(SiteMenuQueryApi.class);
    }

    @Bean
    public WorkingPageCommandApi workingPageCommandApi() {
        return clientApiHelper(WorkingPageCommandApi.class);
    }

    @Bean
    public WorkingPageQueryApi workingPageQueryApi() {
        return clientApiHelper(WorkingPageQueryApi.class);
    }

    @Bean
    public WorkingCardCommandApi workingCardCommandApi() {
        return clientApiHelper(WorkingCardCommandApi.class);
    }

    @Bean
    public WorkingCardQueryApi workingCardQueryApi() {
        return clientApiHelper(WorkingCardQueryApi.class);
    }

    private <T> T clientApiHelper(Class<T> clazz) {
        JAXRSClientFactoryBean clientFactory = new JAXRSClientFactoryBean();
        clientFactory.setAddress(String.format("http://%s:%s/", contentServiceHost, contentServicePort));
        clientFactory.setProvider(jacksonJsonProvider);
        clientFactory.setServiceClass(clazz);
        return (T) clientFactory.create();
    }

}
