package info.earty.gateway.infrastructure.jaxrs.cxf;

//TODO EA 9/7/2021 - expand on the below note
//NOTE EA 9/4/2021 - This interface is needed because trying to use the class
//directly results in exceptions like:
//      The bean 'siteMenuQueryServiceApi' could not be injected as a 'info.earty.gateway.presentation.content.SiteMenuQueryServiceApi'
//      because it is a JDK dynamic proxy that implements: info.earty.content.presentation.SiteMenuQueryApi
public interface CxfServiceApiProxy {
}
