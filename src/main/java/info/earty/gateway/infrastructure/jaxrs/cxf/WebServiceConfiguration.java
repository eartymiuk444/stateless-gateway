package info.earty.gateway.infrastructure.jaxrs.cxf;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import info.earty.gateway.presentation.user.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.cxf.Bus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.openapi.OpenApiCustomizer;
import org.apache.cxf.jaxrs.openapi.OpenApiFeature;
import org.apache.cxf.jaxrs.swagger.ui.SwaggerUiConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Configuration
@RequiredArgsConstructor
public class WebServiceConfiguration {

    private final Bus bus;

    private final JacksonJsonProvider jacksonJsonProvider;

    private final UserService userService;
    private final List<CxfServiceApiProxy> cxfServiceApiProxies;

    @Bean
    public Server rsServer() {
        JAXRSServerFactoryBean endpoint = new JAXRSServerFactoryBean();
        endpoint.setBus(bus);
        endpoint.setAddress("/");
        endpoint.setServiceBeans(Arrays.<Object>asList(
                userService,
                cxfServiceApiProxies
        ));
        endpoint.setProviders(Arrays.<Object>asList(jacksonJsonProvider, this.accessDeniedExceptionMapper()));
        endpoint.setFeatures(Arrays.asList(createOpenOpenApiFeature()));
        return endpoint.create();
    }

    @Bean
    public OpenApiFeature createOpenOpenApiFeature() {
        final OpenApiFeature openApiFeature = new OpenApiFeature();
        openApiFeature.setPrettyPrint(true);
        openApiFeature.setTitle("earty.info api");
        openApiFeature.setContactName("Erik Artymiuk");
        openApiFeature.setDescription("api for earty.info site");
        openApiFeature.setVersion("1.0.0");
        openApiFeature.setResourcePackages(new HashSet<>(Arrays.asList("info.earty.content.presentation",
                "info.earty.image.presentation", "info.earty.attachment.presentation")));
        openApiFeature.setSwaggerUiConfig(new SwaggerUiConfig().url("openapi.json"));

        OpenApiCustomizer openApiCustomizer = new OpenApiCustomizer();
        openApiCustomizer.setDynamicBasePath(true);
        openApiFeature.setCustomizer(openApiCustomizer);

        return openApiFeature;
    }

    @Bean
    public AccessDeniedExceptionMapper accessDeniedExceptionMapper() {
        return new AccessDeniedExceptionMapper();
    }

}
