package info.earty.gateway.infrastructure.spring.security.oidc.custom.token;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import info.earty.gateway.infrastructure.servlet.CookieUtil;
import lombok.Data;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SaveContextOnUpdateOrErrorResponseWrapper;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.time.Instant;
import java.util.Collection;
import java.util.stream.Collectors;

public class EartyInfoAuthSecurityContextRepository implements SecurityContextRepository {

    final public static String AUTH_COOKIE_NAME = "Authentication";

    private String jwtSecret;
    private final ObjectMapper objectMapper;

    public EartyInfoAuthSecurityContextRepository(String jwtSecret) {
      ObjectMapper mapper = new ObjectMapper();
      mapper.registerModule(new JavaTimeModule());
      this.objectMapper = mapper;
      this.jwtSecret = jwtSecret;
    }

    @Override
    public boolean containsContext(HttpServletRequest request) {
        return CookieUtil.getCookie(request, AUTH_COOKIE_NAME).isPresent();
    }

    @Override
    public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {

        HttpServletRequest request = requestResponseHolder.getRequest();
        HttpServletResponse response = requestResponseHolder.getResponse();
        requestResponseHolder.setResponse(new SaveToCookieResponseWrapper(request, response));

        Authentication authentication = CookieUtil.getCookie(requestResponseHolder.getRequest(), AUTH_COOKIE_NAME)
                .map(x -> {
                    EartyInfoOidcAuthToken eartyAuthToken = null;
                    try {
                        eartyAuthToken = verifyAndFromJwt(JWSObject.parse(x.getValue()), objectMapper, this.jwtSecret);
                    } catch (JOSEException | ParseException | IOException e) {
                        throw new RuntimeException(e);
                    }
                    if (eartyAuthToken.getPrincipal().getExpiresAt().isBefore(Instant.now())) {
                        return null;
                    } else {
                        return eartyAuthToken;
                    }
                })
                .orElse(null);
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        if (authentication != null) {
            context.setAuthentication(authentication);
        }
        return context;
    }

    @Override
    public void saveContext(SecurityContext context, HttpServletRequest request, HttpServletResponse response) {
        SaveToCookieResponseWrapper responseWrapper = (SaveToCookieResponseWrapper) response;
        if (!responseWrapper.isContextSaved()) {
            responseWrapper.saveContext(context);
        }
    }

    private class SaveToCookieResponseWrapper extends SaveContextOnUpdateOrErrorResponseWrapper {
        private final HttpServletRequest request;

        SaveToCookieResponseWrapper(HttpServletRequest request, HttpServletResponse response) {
            super(response, true);
            this.request = request;
        }

        @Override
        protected void saveContext(SecurityContext securityContext) {

            HttpServletResponse response = (HttpServletResponse) getResponse();
            final Authentication authentication = securityContext.getAuthentication();

            if (authentication != null) {
                if (authentication instanceof EartyInfoOidcAuthToken) {
                    JWSObject jwsObject = null;
                    try {
                        jwsObject = toSignedJwt((EartyInfoOidcAuthToken) authentication, objectMapper, jwtSecret);
                        String cookieValue = jwsObject.serialize();
                        CookieUtil.addCookie(response, AUTH_COOKIE_NAME, cookieValue);
                    } catch (IOException | JOSEException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }

    private static JWSObject toSignedJwt(EartyInfoOidcAuthToken eartyAuthToken, ObjectMapper objectMapper, String jwtSecret) throws IOException, JOSEException {
        RepositoryDto dto = new RepositoryDto();

        dto.setIssuer(eartyAuthToken.getPrincipal().getIssuer());
        dto.setSubject(eartyAuthToken.getPrincipal().getSubject());
        dto.setExpiresAt(eartyAuthToken.getPrincipal().getExpiresAt());
        dto.setName(eartyAuthToken.getPrincipal().getName());

        dto.setAuthorities(eartyAuthToken.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet()));

        byte[] writtenBytes = objectMapper.writeValueAsBytes(dto);

        JWSObject jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.HS512),
                new Payload(writtenBytes));
        jwsObject.sign(new MACSigner(jwtSecret));

        return jwsObject;
    }

    private static EartyInfoOidcAuthToken verifyAndFromJwt(JWSObject jwsObject, ObjectMapper objectMapper, String jwtSecret) throws JOSEException, IOException {
        JWSVerifier verifier = new MACVerifier(jwtSecret);
        Assert.isTrue(jwsObject.verify(verifier), "JWSObject failed verification.");

        RepositoryDto repositoryDto = objectMapper.readValue(jwsObject.getPayload().toBytes(), RepositoryDto.class);

        EartyInfoOidcUser eartyUser = new EartyInfoOidcUser();
        eartyUser.setIssuer(repositoryDto.getIssuer());
        eartyUser.setSubject(repositoryDto.getSubject());
        eartyUser.setExpiresAt(repositoryDto.getExpiresAt());
        eartyUser.setName(repositoryDto.getName());

        EartyInfoOidcAuthToken eartyAuthToken = new EartyInfoOidcAuthToken();

        eartyAuthToken.setPrincipal(eartyUser);
        eartyAuthToken.setAuthorities(repositoryDto.getAuthorities().stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet()));
        eartyAuthToken.setAuthenticated(true);

        return eartyAuthToken;
    }

    @Data
    public static class RepositoryDto {
        private String subject;
        private URL issuer;
        private Collection<String> authorities;
        private String name;
        private Instant expiresAt;
    }
}
