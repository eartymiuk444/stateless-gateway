package info.earty.gateway.infrastructure.spring.security.oidc.custom.token;

import info.earty.gateway.infrastructure.spring.security.oidc.request.ContinueOrSavedRequestAuthenticationSuccessHandler;
import info.earty.gateway.infrastructure.spring.security.oidc.request.ContinueRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.RequestCache;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EartyInfoAuthenticationSuccessHandler extends ContinueOrSavedRequestAuthenticationSuccessHandler {

    public EartyInfoAuthenticationSuccessHandler(ContinueRepository continueRepository, RequestCache requestCache) {
        super(continueRepository, requestCache);
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException {
        OAuth2AuthenticationToken authenticationToken = (OAuth2AuthenticationToken) authentication;
        EartyInfoOidcAuthToken eartyInfoOidcAuthToken = EartyInfoOidcAuthTokenFactory.create(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(eartyInfoOidcAuthToken);
        super.onAuthenticationSuccess(request, response, authentication);
    }
}
