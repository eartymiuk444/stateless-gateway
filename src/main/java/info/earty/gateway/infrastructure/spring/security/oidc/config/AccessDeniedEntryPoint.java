package info.earty.gateway.infrastructure.spring.security.oidc.config;

import org.springframework.http.HttpHeaders;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//NOTE EA 9/14/2021 - This exception handler should handle any AccessDeniedException that doesn't
//  get picked up by the cxf AccessDeniedExceptionMapper. For example if the 'hasRole' was configured
//  in the WebSecurityConfig as opposed to in a @PreAuthorize annotation.
public class AccessDeniedEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException arg2)
            throws IOException {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            response.setHeader(HttpHeaders.WWW_AUTHENTICATE, "Bearer");
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Requires Authentication");
        }
        else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "Access Denied");
        }
    }

}
