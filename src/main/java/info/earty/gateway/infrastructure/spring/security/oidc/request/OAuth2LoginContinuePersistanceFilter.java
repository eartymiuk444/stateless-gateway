package info.earty.gateway.infrastructure.spring.security.oidc.request;

import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestResolver;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class OAuth2LoginContinuePersistanceFilter extends OncePerRequestFilter {

    private final OAuth2AuthorizationRequestResolver oAuth2AuthorizationRequestResolver;
    private final ContinueRepository continueRepository;

    public OAuth2LoginContinuePersistanceFilter(OAuth2AuthorizationRequestResolver oAuth2AuthorizationRequestResolver,
                                                ContinueRepository continueRepository) {
        this.oAuth2AuthorizationRequestResolver = oAuth2AuthorizationRequestResolver;
        this.continueRepository = continueRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        if (this.oAuth2AuthorizationRequestResolver.resolve(httpServletRequest) != null) {
            this.continueRepository.saveContinueUri(httpServletRequest, httpServletResponse);
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
