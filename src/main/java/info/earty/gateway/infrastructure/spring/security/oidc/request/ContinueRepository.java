package info.earty.gateway.infrastructure.spring.security.oidc.request;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ContinueRepository {
    void saveContinueUri(HttpServletRequest request, HttpServletResponse response);
    String loadContinueUri(HttpServletRequest request);
    String removeContinueUri(HttpServletRequest request, HttpServletResponse response);
}
