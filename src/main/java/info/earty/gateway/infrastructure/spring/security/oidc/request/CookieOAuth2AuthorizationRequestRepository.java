package info.earty.gateway.infrastructure.spring.security.oidc.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import info.earty.gateway.infrastructure.servlet.CookieUtil;
import org.springframework.security.jackson2.CoreJackson2Module;
import org.springframework.security.oauth2.client.jackson2.OAuth2ClientJackson2Module;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;

public class CookieOAuth2AuthorizationRequestRepository implements AuthorizationRequestRepository<OAuth2AuthorizationRequest> {

    final private ObjectMapper objectMapper;
    private final static String AUTHORIZATION_REQUEST_COOKIE = "Authorization-Request";

    public CookieOAuth2AuthorizationRequestRepository() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new CoreJackson2Module());
        objectMapper.registerModule(new OAuth2ClientJackson2Module());
        this.objectMapper = objectMapper;
    }

    @Override
    public OAuth2AuthorizationRequest loadAuthorizationRequest(HttpServletRequest request) {
        Assert.notNull(request, "request cannot be null");

        return CookieUtil.getCookie(request, AUTHORIZATION_REQUEST_COOKIE)
                .map(x -> {
                    try {
                        return objectMapper.readValue(Base64.getDecoder().decode(x.getValue()), OAuth2AuthorizationRequest.class);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .orElse(null);
    }

    @Override
    public void saveAuthorizationRequest(OAuth2AuthorizationRequest authorizationRequest, HttpServletRequest request, HttpServletResponse response) {
        Assert.notNull(request, "request cannot be null");
        Assert.notNull(response, "response cannot be null");
        if (authorizationRequest == null) {
            this.removeAuthorizationRequest(request, response);
            return;
        }
        String state = authorizationRequest.getState();
        Assert.hasText(state, "authorizationRequest.state cannot be empty");

        try {
            CookieUtil.addCookie(response, AUTHORIZATION_REQUEST_COOKIE,
                    Base64.getEncoder().encodeToString(objectMapper.writeValueAsBytes(authorizationRequest)));
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public OAuth2AuthorizationRequest removeAuthorizationRequest(HttpServletRequest request) {
        throw new RuntimeException("Unsupported operation; should use alternative removeAuthorizationRequest(HttpServletRequest, HttpServletResponse)");
    }

    @Override
    public OAuth2AuthorizationRequest removeAuthorizationRequest(HttpServletRequest request, HttpServletResponse response) {
        OAuth2AuthorizationRequest authRequest = loadAuthorizationRequest(request);
        CookieUtil.deleteCookie(request, response, AUTHORIZATION_REQUEST_COOKIE);
        return authRequest;
    }
}
