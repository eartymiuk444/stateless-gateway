package info.earty.gateway.infrastructure.spring.security.oidc.request;

import info.earty.gateway.infrastructure.servlet.CookieUtil;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Base64;
import java.util.Optional;

public class CookieContinueRepository implements ContinueRepository {

    private static final String CONTINUE_COOKIE_NAME = "CONTINUE";
    private static final String CONTINUE_PARAM_NAME = "continue";

    @Override
    public void saveContinueUri(HttpServletRequest request, HttpServletResponse response) {
        String continueParam = request.getParameter(CONTINUE_PARAM_NAME);

        if (continueParam != null) {
            UriComponents uriComponents = UriComponentsBuilder.fromUriString(continueParam).build();

            if (uriComponents.getHost() == null &&
                    uriComponents.getScheme() == null &&
                    uriComponents.getPort() == -1 &&
                    uriComponents.getQuery() == null &&
                    uriComponents.getUserInfo() == null &&
                    uriComponents.getPath() != null) {
                String continueUriString = UriComponentsBuilder.fromPath(uriComponents.getPath()).fragment(uriComponents.getFragment()).build().toString();
                CookieUtil.addCookie(response, CONTINUE_COOKIE_NAME, Base64.getEncoder().encodeToString(continueUriString.getBytes()));
            }
        }
    }

    @Override
    public String loadContinueUri(HttpServletRequest request) {
        Optional<Cookie> continueUriCookie = CookieUtil.getCookie(request, CONTINUE_COOKIE_NAME);
        return continueUriCookie.map(cookie -> new String(Base64.getDecoder().decode(cookie.getValue()))).orElse(null);
    }

    @Override
    public String removeContinueUri(HttpServletRequest request, HttpServletResponse response) {
        String continueUrl = loadContinueUri(request);
        CookieUtil.deleteCookie(request, response, CONTINUE_COOKIE_NAME);
        return continueUrl;
    }

}
