package info.earty.gateway.infrastructure.spring.security.oidc.custom.token;

import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.util.Assert;

import java.util.Collections;

public class EartyInfoOidcAuthTokenFactory {
    public static EartyInfoOidcAuthToken create(OAuth2AuthenticationToken oAuth2AuthenticationToken) {
        Assert.notNull(oAuth2AuthenticationToken, "OAuth2AuthenticationToken is null");
        Assert.isTrue(oAuth2AuthenticationToken.isAuthenticated(), "OAuth2AuthenticationToken is not authenticated");
        Assert.isTrue(oAuth2AuthenticationToken.getPrincipal() instanceof OidcUser, "OAuth2AuthenticationToken's principal is not an OidcUser");

        EartyInfoOidcAuthToken token = new EartyInfoOidcAuthToken();
        token.setPrincipal(new EartyInfoOidcUser((OidcUser)oAuth2AuthenticationToken.getPrincipal()));
        token.setAuthorities(Collections.unmodifiableCollection(oAuth2AuthenticationToken.getAuthorities()));
        token.setAuthenticated(oAuth2AuthenticationToken.isAuthenticated());

        return token;
    }
}
