package info.earty.gateway.infrastructure.spring.security.oidc.custom.token;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import java.net.URL;
import java.security.Principal;
import java.time.Instant;

@Setter(AccessLevel.PACKAGE)
@Getter(AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class EartyInfoOidcUser implements Principal {

    private String subject;
    private URL issuer;
    private String name;
    private Instant expiresAt;

    public EartyInfoOidcUser(OidcUser oidcUser) {
        this.subject = oidcUser.getSubject();
        this.issuer = oidcUser.getIssuer();
        this.name = oidcUser.getName();
        this.expiresAt = oidcUser.getExpiresAt();
    }

    @Override
    public String getName() {
        return name;
    }
}
