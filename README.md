This is a sample project attempting to do stateless authentication using spring security oauth2 login. (See https://docs.spring.io/spring-security/site/docs/5.4.2/reference/html5/#oauth2).

The sample was assuming it'd be used to authenticate a user to a website of mine (earty.info).

This is accomplished by doing a few things: 

* Overriding the default ```AuthorizationRequestRepository``` (which is ```HttpSessionOAuth2AuthorizationRequestRepository```) and using a custom implementation that stores the authorization request in a cookie.
* Overriding the default ```RequestCache``` which I believe is ``` HttpSessionRequestCache``` (not 100% on this) and instead use the ```CookieRequestCache```
* Define a custom extension of the default ```AuthenticationSuccessHandler```, ```SavedRequestAwareAuthenticationSuccessHandler```, that hooks into the result of an oauth 2 login in its success handler and creates a custom ```Authentication``` using the ```OAuth2AuthenticationToken``` and sets this as the authentication on the security context. It then delegates to the super class's default success handler.
    * The idea was to override the ```Authentication``` created from the default spring security implementation of the oauth2 login flow because attempting to serialize the original ```OAuth2AuthenticationToken``` resulted in a large cookie nearly at the typical browser's size limit. 
    * I think this is a semi-hacky way to accomplish this. It would likely be more appropriate to define an entirely new authentication mechanism via a custom filter rather than trying to hook into the result of the ```OAuth2LoginAuthenticationFilter```. At first glance I thought this would require implementing a significant chunk of the oauth2 flow. But looking back perhaps this could be accomplished by simply defining a filter that still utilizes a good chunk of the default oauth2 login process via ```OidcAuthorizationCodeAuthenticationProvider``` just from a custom filter. This would require further exploration. Hooking in via the success handler was easier/quicker.
* Override the default ```SecurityContextRepository```, which I believe is ```HttpSessionSecurityContextRepository```, and define a custom one that stores and loads the custom authentication produced from the custom success handler. It serializes it first using ```com.fasterxml.jackson.databind.ObjectMapper``` and then using ```com.nimbusds.jose.JWSObject``` which also signs it. The repository implementation then verifies the signature when loading it.

This was tested using Google and Microsoft as Oidc providers and seemed to work well. However, using cookies as storage for the session seems to be an anti-pattern that makes this implementation moot. I need to review the criticism surrounding this approach further.

* http://cryto.net/~joepie91/blog/2016/06/13/stop-using-jwt-for-sessions/
* http://cryto.net/%7Ejoepie91/blog/2016/06/19/stop-using-jwt-for-sessions-part-2-why-your-solution-doesnt-work/
* https://www.scottbrady91.com/JOSE/Alternatives-to-JWTs - Misuse of JWTs

I intend to pick this back up in some form. Some things I want to look at when I do:

*  Consider criticism of cookies as storage for sessions and consider the alternative approaches of scaling sessions. (sticky sessions, dedicated redis db, db storage of session data...)
* Figure out how to store client secrets and application secrets without exposing them unnecessarily.
* Assuming I still think session storage using this approach is viable or maybe just for the sake of completing this exploration:
    * Look closer into how to appropriately handle expiration times of the custom authentication cookies.
    * Look closer at alternative JWA (algorithms) for signing and encrypting the cookie. (i.e. HS256 doesn't seem appropriate for reasons listed here: https://www.scottbrady91.com/JOSE/JWTs-Which-Signing-Algorithm-Should-I-Use) and an algorithm using a public/private key pair may be more appropriate.
    * Look into using the ```SignedJWT``` object from nimbus vs ```JWSObject```. May need to look over the JWT specification as I am not even sure the context JWTs are meant to be used. (https://tools.ietf.org/html/rfc7519)
* Assuming I don't think this approach is viable or just to explore alternative options to sessions:
    * Look into switching to stateful sessions using a modified configuration but still using oauth2 login.
    * Look into implementing sticky sessions, db storage, ...
    
An additional note is that this project uses JAX-RS (CXF Implementation). 