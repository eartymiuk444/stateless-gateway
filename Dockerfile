FROM ibm-semeru-runtimes:open-11-jre
COPY target/stateless-gateway-3.0.0.jar stateless-gateway-3.0.0.jar
ENTRYPOINT ["java","-jar","/stateless-gateway-3.0.0.jar"]